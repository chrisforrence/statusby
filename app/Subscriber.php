<?php

namespace App;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscriber extends Model
{
    use HasUuid;
    use SoftDeletes;

    protected $fillable = ['email'];

    public function components()
    {
        return $this->belongsToMany(Component::class, 'components_subscribers');
    }
}
