<?php

namespace App;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Incident extends Model
{
    use HasUuid;
    protected $fillable = ['title', 'classification'];

    public function components()
    {
        return $this->belongsToMany(Component::class, 'components_incidents');
    }

    public function updates()
    {
        return $this->hasMany(Update::class);
    }
}
