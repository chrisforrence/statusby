<?php

namespace App;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    use HasUuid;

    protected $fillable = ['name', 'description', 'is_public'];

    public function subscribers()
    {
        return $this->belongsToMany(Subscriber::class, 'components_subscribers');
    }

    public function incidents()
    {
        return $this->belongsToMany(Incident::class, 'components_incidents');
    }

    public function parent()
    {
        return $this->hasOne(Component::class)->withDefault(null);
    }
}
