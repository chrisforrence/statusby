<?php

namespace App;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Update extends Model
{
    use HasUuid;
    protected $fillable = ['status', 'description'];

    public function incident()
    {
        return $this->belongsTo(Incident::class);
    }
}
