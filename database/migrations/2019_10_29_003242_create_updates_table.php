<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('updates', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('incident_id');
            $table->enum('status', ['investigate', 'identify', 'fix', 'monitor', 'resolve']);
            $table->text('description');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('incident_id')->references('id')->on('incidents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('updates');
    }
}
